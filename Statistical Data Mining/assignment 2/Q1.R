library(ISLR)
data("USArrests")
dim(USArrests)
summary(USArrests)
head(USArrests)
#a
clust=hclust(dist(USArrests),method="complete")
x11()
plot(clust,main = "Complete Clustering of USArrests", xlab = "States",ylab = "Distance")

#b
cutmodel=cutree(clust,k=3)


cutmodel[cutmodel==1]
cutmodel[cutmodel==2]
cutmodel[cutmodel==3]

rect.hclust(clust,k=3)


#c
scaled_data=scale(USArrests)
scaled_clust=hclust(dist(scaled_data),method="complete")
x11()
plot(scaled_clust,main = "Complete Clustering of Scaled USArrests_data", xlab = "States",ylab = "Distance")

scaled_cutmodel=cutree(scaled_clust,k=3)
scaled_cutmodel

scaled_cutmodel[scaled_cutmodel==1]
scaled_cutmodel[scaled_cutmodel==2]
scaled_cutmodel[scaled_cutmodel==3]

rect.hclust(scaled_clust,k=3)

table(cutmodel)
table(scaled_cutmodel)
