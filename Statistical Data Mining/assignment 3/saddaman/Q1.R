library(ElemStatLearn)
data(nci)
head(nci)
#View(nci)
summary(nci)
library(kohonen)
nci_scale=scale(nci)

# fit an SOM
set.seed(923)
som_grid <- somgrid(xdim = 5, ydim = 5, topo = "hexagonal")
nci.som <- som(nci_scale,grid=som_grid, rlen = 3000)

codes <- nci.som$codes[[1]]

x11()
plot(nci.som,type="codes", main = "Tumor MicroArray Data")

x11()
plot(nci.som, type = "changes", main = "Tumor MicroArray Data")

x11()
plot(nci.som, type = "count", main = "Tumor MicroArray Data")

x11()
plot(nci.som, type = "mapping", main = "Tumor MicroArray Data")

coolBlueHotRed <- function(n, alpha = 1){rainbow(n, end=4/6, alpha = alpha)[n:1]}

x11()
plot(nci.som, type = "dist.neighbours", palette.name = coolBlueHotRed)

# component plane plots
for (i in 1:13){
  x11()
  plot(nci.som, type = "property", property=codes[,i], main = colnames(codes)[i])
}


d <- dist(codes)
hc <- hclust(d,method="complete")

x11()
plot(hc)
rect.hclust(hc,k=3)
som_cluster <- cutree(hc,k=3)

# plot the SOM with the found clusters

my_pal <- c("red", "blue", "yellow")
my_bhcol <- my_pal[som_cluster]

graphics.off()

x11()
plot(nci.som, type = "mapping", col = "black", bgcol = my_bhcol)
add.cluster.boundaries(nci.som, som_cluster)

