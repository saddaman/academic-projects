
(with temp(emp_no,title,from_date,to_date) as 
(select * from titles)
(select a.title as src, b.title as dst,avg(datediff(b.from_date,a.from_date)) as years from temp a cross join temp b 
on a.emp_no=b.emp_no and a.title<>b.title and a.from_date<b.from_date group by src,dst))   order by src,dst;