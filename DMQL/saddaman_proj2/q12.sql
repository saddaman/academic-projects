with temp(dept_name,avg_sal,cnt) as 
(select dept_name, avg(salary)as avg_sal ,count(d.emp_no) as cnt  
from (select emp_no,b.dept_name from current_dept_emp a inner join departments b on a.dept_no=b.dept_no) d 
 inner join 
 (select emp_no,salary from salaries where year(to_date)=9999) s on s.emp_no=d.emp_no 
 group by dept_name) 
select temp.dept_name,count(emp_no)/cnt*100 as above_avg_pect from temp inner join 
(select a.emp_no,b.dept_name,s.salary from current_dept_emp a inner join departments b on a.dept_no=b.dept_no 
inner join (select emp_no,salary from salaries where year(to_date)=9999) s on s.emp_no=a.emp_no ) gen 
on gen.dept_name=temp.dept_name and gen.salary>avg_sal 
group by temp.dept_name order by temp.dept_name;