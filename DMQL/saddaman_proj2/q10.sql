select eh.emp_no as h_empno,(eh.salary) as h_salary,eh.hire_date as h_date,el.emp_no as l_empno,(el.salary) as l_salary,el.hire_date as l_date 
from 
(select employees.emp_no, salary,hire_date from employees,salaries where employees.emp_no=salaries.emp_no and year(employees.birth_date)=1965 and year(salaries.to_date)=9999 
) eh 
inner join (select employees.emp_no, salary,hire_date from employees,salaries where employees.emp_no=salaries.emp_no and year(employees.birth_date)=1965 and year(salaries.to_date)=9999 
) el 
on eh.hire_date>el.hire_date and (eh.salary)>(el.salary) order by eh.emp_no,el.emp_no;