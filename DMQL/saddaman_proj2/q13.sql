with temp(emp_no,title,from_date,to_date) as 
(select * from titles)
select   a.title as src, b.title as dst from temp a cross join temp b 
on a.emp_no=b.emp_no  and a.to_date<>b.from_date  and a.from_date>=b.from_date group by src,dst order by src,dst;