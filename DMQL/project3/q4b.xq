let $books:=distinct-values(doc("books.xml")/biblio/author/book/title)
for $bk in $books,
$x in distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/rating)
let $pr:=distinct-values(doc("books.xml")/biblio/author/book[rating=$x]/rating)
let $tit:=distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/title)
order by xs:float($x) descending
return (<title>{$tit}</title>,<rating>{$pr}</rating>)