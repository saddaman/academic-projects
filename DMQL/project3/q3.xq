<result><categories><output>{
let $books:=distinct-values(doc("books.xml")/biblio/author/book/title)
let $category:=distinct-values(doc("books.xml")/biblio/author/book/category)
let $gbavg:=avg(
for $bk in $books,
$x in distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/price)
let $pr:=distinct-values(doc("books.xml")/biblio/author/book[price=$x]/price)
let $tit:=distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/title)
return (<price>{$pr}</price>))


for $cat in $category,
$bk in doc("books.xml")/biblio/author/book[category=$cat]

group by $cat
return if(avg(distinct-values($bk/price))>$gbavg) then (<category>{distinct-values(doc("books.xml")/biblio/author/book[category=$cat and price=max(//price)]/category)}</category>,
<title>{
distinct-values(doc("books.xml")/biblio/author/book[category=$cat and price=max(//price)]/title)}</title>,
<price>{
distinct-values(doc("books.xml")/biblio/author/book[category=$cat and price=max(//price)]/price)}</price>,

for $a in distinct-values(doc("books.xml")/biblio/author[book/category=$cat and book/price=max(//book/price) and book/title=(doc("books.xml")/biblio/author/book[category=$cat and price=max(//price)]/title)]/name)
return <name>{$a}</name>


)
}
</output></categories></result>