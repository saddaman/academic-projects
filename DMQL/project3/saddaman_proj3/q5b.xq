let $books:=distinct-values(doc("books.xml")/biblio/author/book/title)
for $bk in $books,
$x in distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/rating)
let $rat:=distinct-values(doc("books.xml")/biblio/author/book[rating=$x]/rating)
let $cat:=distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/category) 
group by $cat
return (<title>{distinct-values(doc("books.xml")/biblio/author/book[title=$bk and rating=max($rat)]/title)[1]}</title>,<rating>{max($rat)}</rating>,<category>{$cat}</category>)