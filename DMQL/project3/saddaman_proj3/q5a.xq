let $books:=distinct-values(doc("books.xml")/biblio/author/book/title)
for $bk in $books,
$x in distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/price)
let $pr:=distinct-values(doc("books.xml")/biblio/author/book[price=$x]/price)
let $cat:=distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/category) 
group by $cat
return (<title>{distinct-values(doc("books.xml")/biblio/author/book[title=$bk and price=min($pr)]/title)}</title>,<price>{min($pr)}</price>,<category>{$cat}</category>)