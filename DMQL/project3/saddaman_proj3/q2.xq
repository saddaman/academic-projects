let $books:=distinct-values(doc("books.xml")/biblio/author/book/title)
let $mas:=(
for $bk in $books,
$prod in doc("books.xml")/biblio/author,
$item in doc("books.xml")/biblio/author[name!=$prod/name]


let $a1:=$prod/name
let $a2:=$item/name
where $item/book/title=$bk and $prod/book/title=$bk and $item/name!=$prod/name 
group by $a1, $a2

return <output>
 {$item/name}
 {$prod/name}
 {$prod/book[title=$bk]}
</output>
)
return <coauthor>{
let $xm:=(
for $f in $mas
where count(distinct-values($f/book/title))>1

return $f)
return $xm[1] 
}
</coauthor>