let $books:=distinct-values(doc("books.xml")/biblio/author/book/title)
let $cpr:=distinct-values(
for $bk in $books,
$x in distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/price)
let $pr:=distinct-values(doc("books.xml")/biblio/author/book[price=$x]/price)
let $cat:=distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/category) 
group by $cat
return <title>{distinct-values(doc("books.xml")/biblio/author/book[title=$bk and price=min($pr)]/title)}</title>)
let $crat:=distinct-values(
for $bk in $books,
$x in distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/rating)
let $rat:=distinct-values(doc("books.xml")/biblio/author/book[rating=$x]/rating)
let $cat:=distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/category) 
group by $cat
return <title>{distinct-values(doc("books.xml")/biblio/author/book[title=$bk and rating=max($rat)]/title)[1]}</title>)
let $tpr:=xs:integer(0)
for $x in $crat

return <output>{(<title>{$x}</title>,<category>{distinct-values(doc("books.xml")/biblio/author/book[title=$x]/category)}</category>,
for $y in $cpr
where distinct-values(doc("books.xml")/biblio/author/book[title=$x]/category)!=distinct-values(doc("books.xml")/biblio/author/book[title=$y]/category)
return (<title>{$y}</title>,<category>{distinct-values(doc("books.xml")/biblio/author/book[title=$y]/category)}</category>)
)}</output>