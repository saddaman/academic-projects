let $path:=doc("books.xml")/biblio/author
let $books:=distinct-values($path/book/title)
return <biblio>{
for $bk in $books
let $cat:=distinct-values($path/book[title=$bk]/category)
let $rat:=distinct-values($path/book[title=$bk]/rating)
let $pr:=distinct-values($path/book[title=$bk]/price)
let $yr:=distinct-values(data($path/book[title=$bk]/@year))
return (<book year='{$yr}'>
<title>{$bk}</title>
<rating>{$rat}</rating>
<category>{$cat}</category>
<price>{$pr}</price>
<authors>
{for $nam in distinct-values($path[book/title=$bk]/name)
return <author>{$nam}</author>}</authors>
</book>

)
}</biblio>