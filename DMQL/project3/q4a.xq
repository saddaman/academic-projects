let $books:=distinct-values(doc("books.xml")/biblio/author/book/title)
for $bk in $books,
$x in distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/price)
let $pr:=distinct-values(doc("books.xml")/biblio/author/book[price=$x]/price)
let $tit:=distinct-values(doc("books.xml")/biblio/author/book[title=$bk]/title)
order by xs:integer($x) 
return (<title>{$tit}</title>,<price>{$pr}</price>)