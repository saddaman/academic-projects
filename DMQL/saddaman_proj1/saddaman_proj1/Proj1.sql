create database dmql;
use dmql;
create table User_Login(EmailID varchar(225) PRIMARY KEY, Password varchar(225), AccountType varchar(225));
create table Department(DeptID varchar(225) PRIMARY KEY, Name varchar(225), Year int, Semester varchar(225));
create table Staff(StaffID varchar(225) PRIMARY KEY, Name varchar(225) UNIQUE, DeptID varchar(225), EmailID varchar(225), FOREIGN KEY (DeptID) references Department(DeptID), FOREIGN KEY (EmailID) references User_Login(EmailID) ) ;
create table Professor(ProfessorID varchar(225) PRIMARY KEY, Name varchar(225) UNIQUE, DeptID varchar(225), EmailID varchar(225), FOREIGN KEY (DeptID) references Department(DeptID), FOREIGN KEY (EmailID) references User_Login(EmailID) ) ;
create table Student(StudentID varchar(225), Name varchar(225) UNIQUE, DeptID varchar(225), EmailID varchar(225), Semester varchar(225), FOREIGN KEY (DeptID) references Department(DeptID), FOREIGN KEY (EmailID) references User_Login(EmailID), constraint pk_s PRIMARY KEY(StudentID,Semester) ) ;
create table Course(CourseID varchar(225) PRIMARY KEY, Name varchar(225), Prerequisite varchar(225), DeptID varchar(225), FOREIGN KEY (DeptID) references Department(DeptID));
create table Course_Semester(CourseID varchar(225), Semester varchar(225), Year int, Instructor varchar(225), TeachingAssistant varchar(225), FOREIGN KEY (CourseID) references Course(CourseID), FOREIGN KEY (Instructor) references Professor(ProfessorID), FOREIGN KEY (TeachingAssistant) references Student(StudentID));
create table Course_Registration(CourseID varchar(225), Semester varchar(225), Year int, PrerequisiteScore varchar(2) NOT NULL, Feedback varchar(225), FinalGrade varchar(2), StudentID varchar(225), FOREIGN KEY (CourseID) references Course(CourseID), FOREIGN KEY (StudentID,Semester) references Student(StudentID,Semester), CHECK(PrerequisiteScore<>'F'));
create table Exam(ExamID varchar(225), StudentID varchar(225), ExamScore int, CourseID varchar(225), FOREIGN KEY (CourseID) references Course(CourseID), FOREIGN KEY (StudentID) references Student(StudentID));
create table BifurcatedScores(ExamID varchar(225), StudentID varchar(225), QuestionNo varchar(5), Marks int, FOREIGN KEY (StudentID) references Student(StudentID));

ALTER table Course_Semester add column capacity int not null;
Alter table Exam add column Semester varchar(225);
Alter table Exam add column Year int;
alter table Exam modify column ExamScore varchar(4);
show tables;