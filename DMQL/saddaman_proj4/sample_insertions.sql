insert into User_Login values('saddaman@buffalo.edu','123wer','Student');
insert into User_Login values('prof@buffalo.edu','343gdfsd','Professor');
insert into User_Login values('staff@buffalo.edu','54354fdfdf','Staff');
insert into User_Login values('apshreyas@buffalo.edu','123wer','Student');
insert into User_Login values('shreyas@buffalo.edu','123wer','Student');

insert into Department values('CSE','Computer Science',2020,'Spring 2020');

insert into Staff values('St1','Alexander','CSE','staff@buffalo.edu');

insert into Professor values('Pf1','Louis','CSE','prof@buffalo.edu');

insert into Student values('Stud1','Shreyas','CSE','saddaman@buffalo.edu','Fall 2019');
insert into Student values('Stud2','AP_Shreyas','CSE','apshreyas@buffalo.edu','Spring 2020');
insert into Student values('Stud3','Shreyas_KL','CSE','shreyas@buffalo.edu','Spring 2020');

insert into Course values('CSE 560','DMQL','CSE 360','CSE');



insert into Course_Registration values('CSE 560','Spring 2020',2020,'B+',null,null,'Stud3');



