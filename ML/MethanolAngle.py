import numpy as np
#import pandas as pd
import math

class methanol:
    data={
      'H1':[1.2001,0.0363,0.8431],
      'C':[0.7031,0.0083,-0.1305],
      'H2':[0.9877,0.8943,-0.7114],
      'H3':[1.0155,-0.8918,-0.6742],
      'O':[-0.6582,-0.0067,0.1730],
      'H4':[-1.1326,-0.0311,-0.6482],
      
      }
    
    
    def neighbors(self,list1,list2):
        l1=np.array(list1)
        l2=np.array(list2)
        l3=l2-l1
        l4=(np.sum(l3**2))**0.5
        if l4<1.5:
            return True
        else:
            return False
    
    def findangle(self,list1,list2,list3):
        l1=np.array(list1)
        l2=np.array(list2)
        l3=np.array(list3)
        vector1=l1-l2
        vector2=l3-l2
        numerator=np.dot(vector1,vector2)
        denominator=math.sqrt(np.sum(vector1**2))*math.sqrt(np.sum(vector2**2))
        angle=math.acos(numerator/denominator)
        
        return math.degrees(angle)




if __name__ == '__main__':
    meth=methanol()
    data=meth.data
    if meth.neighbors(data['C'], data['O']):
        print('C and O are bonded neigbors')
    if meth.neighbors(data['C'], data['H1']):
        print('C and H1 are bonded neigbors')
    if meth.neighbors(data['C'], data['H2']):
        print('C and H2 are bonded neigbors')
    if meth.neighbors(data['C'], data['H3']):
        print('C and H3 are bonded neigbors')
    if meth.neighbors(data['C'], data['H4']):
        print('C and H4 are bonded neigbors')
    if meth.neighbors(data['H1'], data['O']):
        print('O and H1 are bonded neigbors')
        angl=meth.findangle(data['C'], data['O'], data['H1'])
        print('Angle between C-O-H is '+str(angl))
    if meth.neighbors(data['H2'], data['O']):
        print('O and H2 are bonded neigbors')
        angl=meth.findangle(data['C'], data['O'], data['H2'])
        print('Angle between C-O-H is '+str(angl))
    if meth.neighbors(data['H3'], data['O']):
        print('O and H3 are bonded neigbors')
        angl=meth.findangle(data['C'], data['O'], data['H3'])
        print('Angle between C-O-H is '+str(angl))
    if meth.neighbors(data['H4'], data['O']):
        print('O and H4 are bonded neigbors')
        angl=meth.findangle(data['C'], data['O'], data['H4'])
        print('Angle between C-O-H is '+str(angl))
        
        
        