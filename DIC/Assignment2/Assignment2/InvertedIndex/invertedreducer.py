#!/usr/bin/env python3
from operator import itemgetter
import sys

index={}
for line in sys.stdin:
    line=line.strip()
    
    word,filename,count=line.split('\t')
    if word in index:
        if filename in index[word]:
            index[word][filename]+=int(count)
        else:
            index[word]={}
            index[word][filename]=int(count)
    else:
        index[word]={}
        index[word][filename]=int(count)
    
    
for k,v in index.items():
    print(k,v)