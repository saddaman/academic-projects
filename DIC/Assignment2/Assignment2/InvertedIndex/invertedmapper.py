#!/usr/bin/env python3
#Reference: https://forums.aws.amazon.com/thread.jspa?threadID=33112

import sys 
import os 
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import RegexpTokenizer

#removing stop words
stop_words = set(stopwords.words("english"))
tokenizer = RegexpTokenizer(r'\w+')
stem = PorterStemmer()

for line in sys.stdin:
    line = line.strip()
    words=tokenizer.tokenize(line)
    filepath = os.environ["map_input_file"]
    filename = os.path.split(filepath)[-1]
        
    for word in words:
        if word in stop_words:
            continue
        else:
            word=stem.stem(word)
            print("%s\t%s\t%s" % (word.lower(), filename,'1'))
        
        