
########################################################################################################################################################################
############################################################################## ASSIGNMENT 2 ############################################################################
########################################################################################################################################################################

# GROUP MEMBERS : Naveen Balaraju & Shreyas Addamane Pallathadka

# Loading dependent libraries

import sys
from operator import itemgetter

word = None
cur_w = None
cur_cnt = 0

for a in sys.stdin:
    a = a.strip()
    word, cnt = a.split(',', 1)
    try:
        cnt = int(cnt)
    except ValueError:
        continue

    if cur_w == word:
        cur_cnt += cnt
    else:
        if cur_w:
            print(cur_w, cur_cnt)
        cur_cnt = cnt
        cur_w = word

if cur_w == word:
    print(cur_w, cur_cnt)
