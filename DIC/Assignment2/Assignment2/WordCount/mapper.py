
########################################################################################################################################################################
############################################################################## ASSIGNMENT 2 ############################################################################
########################################################################################################################################################################

# GROUP MEMBERS : Naveen Balaraju & Shreyas Addamane Pallathadka

# Loading dependent libraries
import string
import sys
from nltk import word_tokenize, sent_tokenize, pos_tag
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer

# Tokenize - converting sentenses into individual words


def tokenize_text(input_text):
    tokenizer=RegexpTokenizer(r'\w+')
    return tokenizer.tokenize(input_text)

# Lemmatize - reducing words to its root form taking part of speech into account


def lemmatize(input_words):
    lemma = WordNetLemmatizer()
    return [lemma.lemmatize(i, j[0].lower()) if j[0].lower() in [
        'a', 'n', 'v'] else lemma.lemmatize(i) for i, j in pos_tag(input_words)]


def main_func():
    for line in sys.stdin:
        token_txt = tokenize_text(line)
        lemma_txt = lemmatize(token_txt)
        for word in lemma_txt:
            print(word+','+ '1')


if __name__ == "__main__":
    main_func()
