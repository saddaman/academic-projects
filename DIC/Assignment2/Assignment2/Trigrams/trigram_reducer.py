
########################################################################################################################################################################
############################################################################## ASSIGNMENT 2 ############################################################################
########################################################################################################################################################################

# GROUP MEMBERS : Naveen Balaraju & Shreyas Addamane Pallathadka

# Loading dependent libraries

#!/usr/bin/env python3
from operator import itemgetter
import sys

index = {}
for line in sys.stdin:
    line = line.strip()
    word, count = line.split('\t')
    if word in index:
        index[word] += int(count)

    else:
        index[word] = int(count)
counter = 0
for k, v in sorted(index.items(), key=lambda item: item[1], reverse=True):
    print(k, v)
    counter += 1
    if counter == 10:
        break

