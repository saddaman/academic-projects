
#!/usr/bin/env python3
import string
import sys
from nltk import word_tokenize, sent_tokenize, pos_tag
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from nltk.collocations import ngrams



def tokenize_text(input_text):
    tokenizer = RegexpTokenizer(r'\w+')
    return tokenizer.tokenize(input_text)

# Lemmatize - reducing words to its root form taking part of speech into account


def lemmatize(input_words):
    lemma = WordNetLemmatizer()
    return [lemma.lemmatize(i, j[0].lower()) if j[0].lower() in ['a', 'n', 'v'] else lemma.lemmatize(i) for i, j in pos_tag(input_words)]


def main_func():
    for line in sys.stdin:
        token_txt = tokenize_text(line)
        lemma_txt = lemmatize(token_txt)
        trigramwords = list(ngrams(lemma_txt, 3))

        for sen in trigramwords:
            sen = list(sen)
            trigram = ''
            for idx, word in enumerate(sen):
                if word == 'sea' or word == 'science' or word == 'fire':
                    sen[idx] = '$'
                else:
                    continue
            if sen[0] == '$' or sen[1] == '$' or sen[2] == '$':
                print(sen[0].lower() + '_' + sen[1].lower() + '_' + sen[2].lower() + '\t' + '1')


if __name__ == "__main__":
    main_func()

