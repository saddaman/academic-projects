#!/usr/bin/env python3
import sys
sep='\t'
for line in sys.stdin:
    line=line.strip()
    line=line.split(",")
    
    if len(line)==2:
        print(line[0],sep,line[1])
    else:
        print(line[0],sep,line[1],sep,line[2],sep,line[3])