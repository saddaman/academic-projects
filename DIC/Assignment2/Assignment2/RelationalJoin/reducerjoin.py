#!/usr/bin/env python3
import sys
sep='\t'
empid = {}

for line in sys.stdin:
    line=line.strip()
    emp_id,val=line.split('\t',1)
    
    if emp_id in empid:
        empid[emp_id]+=val.split('\t')
    else:
        empid[emp_id]=[]
        empid[emp_id]=val.split('\t')

for emp_id in empid.keys():
    print(emp_id,empid[emp_id])    
    