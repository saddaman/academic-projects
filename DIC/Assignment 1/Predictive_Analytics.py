# -*- coding: utf-8 -*-
"""
Predicitve_Analytics.py
"""
import numpy as np
#Reference: https://stats.stackexchange.com/questions/21551/how-to-compute-precision-recall-for-multiclass-multilabel-classification/121939#121939
def Accuracy(y_true,y_pred):
    """
    :type y_true: numpy.ndarray
    :type y_pred: numpy.ndarray
    :rtype: float
    
    """
    confusion_matrix=ConfusionMatrix(y_true,y_pred)
    true_positive=np.diag(confusion_matrix)
    false_positive=np.sum(np.sum(confusion_matrix,axis=0)-true_positive)
    #false_negative=np.sum(np.sum(confusion_matrix,axis=1)-true_positive)
    return np.sum(true_positive)/(np.sum(true_positive)+false_positive)

def Recall(y_true,y_pred):
    """
    :type y_true: numpy.ndarray
    :type y_pred: numpy.ndarray
    :rtype: float
    """
    confusion_matrix=ConfusionMatrix(y_true,y_pred)
    true_positive=np.diag(confusion_matrix)
    #false_positive=np.sum(np.sum(confusion_matrix,axis=0)-true_positive)
    false_negative=(np.sum(confusion_matrix,axis=1)-true_positive)
    return (true_positive/(true_positive+false_negative)) #Broadcasting
    
    
    
    
    
def Precision(y_true,y_pred):
    """
    :type y_true: numpy.ndarray
    :type y_pred: numpy.ndarray
    :rtype: float
    """
    confusion_matrix=ConfusionMatrix(y_true,y_pred)
    true_positive=np.diag(confusion_matrix)
    false_positive=(np.sum(confusion_matrix,axis=0)-true_positive)
    #false_negative=np.sum(np.sum(confusion_matrix,axis=1)-true_positive)
    return (true_positive/(true_positive+false_positive)) #Broadcasting
    

#Ref: https://towardsdatascience.com/machine-learning-algorithms-part-9-k-means-example-in-python-f2ad05ed5203    
def WCSS(Clusters):
    """
    :Clusters List[numpy.ndarray]
    :rtype: float
    """
    def distance(row_1,row_2):
        dis=sum((row_1-row_2)**2)
        return dis
    
    def ClusterCentre(cl):
        centre=list()
        for i in cl:
            centre.append(np.average(i,axis=0))
        return centre
    
    wcss=0
    for i in Clusters:
        centroid=ClusterCentre(Clusters)
        z=0
        for j in i:
            wcss+=distance(j,centroid[z])
        z+=1
    
    return wcss
            
        
    
def ConfusionMatrix(y_true,y_pred):
    
    """
    :type y_true: numpy.ndarray
    :type y_pred: numpy.ndarray
    :rtype: float
    """
    #Reference: https://stackoverflow.com/questions/2148543/how-to-write-a-confusion-matrix-in-python/48087308#48087308
    K = len(np.unique(y_true))  
    unique = sorted(set(y_true))
    matrix = np.zeros((K, K))
    imap   = {key: i for i, key in enumerate(unique)}
    
    for p, a in zip(y_true, y_pred):
        matrix[imap[p]][imap[a]] += 1

    return matrix

def KNN(X_train,X_test,Y_train):
    
    
    Y_test=list()
    def distance(row_1,row_2):
        dis=sum((row_1-row_2)**2)
        return dis**0.5
    
    def neighbor(X_train,Y_train,row):
        neigh=list()
        for i in range(X_train.shape[0]):
            temp_dist=distance(X_train[i],row)
            neigh.append([temp_dist,Y_train[i]])
        neigh.sort(key=lambda x:x[0])  #Reference https://docs.python.org/3/howto/sorting.html
        neighbor=list()
        for i in range(len(neigh)):
            neighbor.append(neigh[i][1])
        
        return neighbor
    
    for i in range(X_test.shape[0]):
        neigh=neighbor(X_train,Y_train,X_test[i])
        lst=max(set(neigh[0:11]), key=neigh[0:11].count)  #Ref: https://stackoverflow.com/questions/10797819/finding-the-mode-of-a-list
        Y_test.append(lst)
        
    
    return  np.array(Y_test)
                
        
        
            
            
        
        
        
    
def RandomForest(X_train,Y_train,X_test):
    """
    :type X_train: numpy.ndarray
    :type X_test: numpy.ndarray
    :type Y_train: numpy.ndarray
    
    :rtype: numpy.ndarray
    """
    
def PCA(X_train,N):
    """
    :type X_train: numpy.ndarray
    :type N: int
    :rtype: numpy.ndarray
    """
    #Reference: https://stackoverflow.com/questions/8092920/sort-eigenvalues-and-associated-eigenvectors-after-using-numpy-linalg-eig-in-pyt
    avg=np.average(X_train,axis=0)
    centre=X_train-avg
    covariance_matrix=np.cov(centre.T)
    eigen_values,eigen_vectors=np.linalg.eig(covariance_matrix)
    index=eigen_values.argsort()[::-1]   
    eigen_values=eigen_values[index]
    eigen_vectors=eigen_vectors[:,index]
    eigen_vectors=eigen_vectors[:,0:N]
    return eigen_vectors.T.dot(centre.T)
    
    
def Kmeans(X_train,N):
    """
    :type X_train: numpy.ndarray
    :type N: int
    :rtype: List[numpy.ndarray]
    """
    def distance(row_1,row_2):
        dis=sum((row_1-row_2)**2)
        return dis**0.5
    
    def ClusterAssignment(X_train,Centroid):
        cl=dict()
        for i in range(len(Centroid)):
            cl[i]=[]
        for i in X_train:
            cl[(np.argmin([distance(i,j) for j in Centroid]))].append(i)
        return cl
    
    def ClusterCentre(cl):
        centre=list()
        for i in cl.values():
            centre.append(np.average(i,axis=0))
        return centre
    
    Centroid=list()
    for i in range(N):
        Centroid.append(X_train[i])
    new_Centroid=list()
    while True:
        new_Centroid=Centroid
        Clusters=ClusterAssignment(X_train,Centroid)
        Centroid=ClusterCentre(Clusters)
        if np.sum(np.array(new_Centroid)-np.array(Centroid))==0:
            break
    l=list()
    for i in Clusters.values():
        l.append(np.array(i))
    
    return l
    
    
    

def SklearnSupervisedLearning(X_train,Y_train,X_test):
    """
    :type X_train: numpy.ndarray
    :type X_test: numpy.ndarray
    :type Y_train: numpy.ndarray
    
    :rtype: List[numpy.ndarray] 
    """

def SklearnVotingClassifier(X_train,Y_train,X_test):
    
    """
    :type X_train: numpy.ndarray
    :type X_test: numpy.ndarray
    :type Y_train: numpy.ndarray
    
    :rtype: List[numpy.ndarray] 
    """


"""
Create your own custom functions for Matplotlib visualization of hyperparameter search. 
Make sure that plots are labeled and proper legends are used
"""
X = np.array([[1, 2],
              [1.5, 1.8],
              [5, 8 ],
              [8, 8],
              [1, 0.6],
              [9,11],
              [1,3],
              [8,9],
              [0,3],
              [5,4],
              [6,4],])
cluster=Kmeans(X,2)
vak=WCSS(cluster)
xya=PCA(X,1)



#from sklearn.metrics import confusion_matrix

gt = [1,1,2,2,1,0]
pd = [1,1,1,1,2,0]

#cm = confusion_matrix(gt, pd)

#rows = gt, col = pred

#compute tp, tp_and_fn and tp_and_fp w.r.t all classes
#tp_and_fn = cm.sum(1)
#tp_and_fp = cm.sum(0)
#tp = cm.diagonal()
acc=Accuracy(gt,pd)
pr = Precision(gt,pd)
re = Recall(gt,pd)

